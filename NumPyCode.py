import numpy as np
arr = np.array([0, 2, 8, 4, 6]) #NumPy ndarray object using array() function
print(arr)

arr_tuple = np.array((1, 2, 3, 4, 5)) #tuple to create a NumPy array
print(arr_tuple)

Zero_D_arr = np.array(42) #0-D array with value 42
print(Zero_D_arr)

One_d_arr = np.array([1, 2, 3, 4, 5]) #1-D array
print(One_d_arr)
print(One_d_arr[1] + One_d_arr[3]) #Acessing using Index and adding them

two_d_arr = np.array([[1, 2, 3], [4, 5, 6]]) #2-D array 
print(two_d_arr)

three_arr = np.array([[[1, 2, 3], [4, 5, 6]], [[1, 2, 3], [4, 5, 6]]]) #3-D array
print(three_arr)
print("element",three_arr[0,1,2]) #Accessing 3-D Array Element

dimen_arr = np.array([1, 2, 3, 4], ndmin=5) #array with 5 dimensions
print(dimen_arr)
print('number of dimensions :', dimen_arr.ndim) #verify that it has 5 dimensions

print(Zero_D_arr.ndim)
print(two_d_arr.ndim)
print(One_d_arr.ndim)
print(three_arr.ndim)